class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price

    def get_total_quantity_of_products(self):
        total_quantity = 0
        for product in self.products:
            total_quantity += product.quantity
        return total_quantity

    def purchase(self):
        self.purchased = True

    # An additional method to print all order details (for console script checking)
    def print_order(self):
        status = "purchased" if self.purchased else "not purchased"
        print(f'--Purchaser: {self.customer_email}--')
        print(f'Order status: {status}')
        print(f'Name\tQuantity\tUnit price\tPrice')
        for product in self.products:
            print(f'{product.name}\t{product.quantity}\t{product.unit_price}\t{product.get_price()}')
        print('Total quantity:', self.get_total_quantity_of_products())
        print('Total price:', self.get_total_price())


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity

    # An additional method to print all product details (for console script checking)
    def print_product_details(self):
        print('--Product details--')
        print(f'Name: {self.name}\nQuantity: {self.quantity}\nUnit price: {self.unit_price}\nPrice: {self.get_price()}')


if __name__ == '__main__':
    # Class: product
    shoes = Product('Shoes', 30.00, 3.0)
    tshirt = Product('T-Shirt', 50.00, 2.0)
    bag = Product('Bag', 10.00)
    shoes.print_product_details()

    # Class: order
    kate_order = Order('kate@example.com')
    kate_order.add_product(shoes)
    kate_order.add_product(tshirt)
    kate_order.add_product(bag)
    kate_order.print_order()
    kate_order.purchase()
    kate_order.print_order()
